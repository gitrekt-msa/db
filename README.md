# Databases

### Milestone #1
- SQL & NoSQL Schemas
- Dummy Data

### Running Container

Build Container

1. `docker build -t gr-psql .`

Run container with all `*.sql` files

2. `docker run --rm  -it -p 5432:5432 gr-psql`

### Resources

[VIDEOS] The Complete SQL bootcamp
[https://www.freetutorials.eu/the-complete-sql-bootcamp-3/](https://www.freetutorials.eu/the-complete-sql-bootcamp-3/)

[WRITTEN] Tutorial
[http://www.postgresqltutorial.com/](http://www.postgresqltutorial.com/)
