CREATE OR REPLACE PROCEDURE Certify_user()
    LANGUAGE SQL
AS
$$
UPDATE users
set certified = true
where id = (SELECT a.user_id
            from (SELECT user_id, count(*) from followers group by user_id having count(*) > 0) a
                     inner join
                 (SELECT user_id, count(*)
                  from question_best_answers
                           join answers ON answer_id = id
                  group by user_id
                  having count(*) > 0) b
                 on a.user_id = b.user_id)
$$;

