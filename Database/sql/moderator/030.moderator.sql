-- CREATE MODERATOR TABLE(s)
CREATE TABLE IF NOT EXISTS moderators(
  username varchar(30) PRIMARY KEY,
  password  varchar(512)
);
