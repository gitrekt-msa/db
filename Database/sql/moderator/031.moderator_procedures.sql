
-- Insert Moderator
CREATE OR REPLACE PROCEDURE Insert_Moderator(
  IN username_val varchar(30),
  IN password_val varchar(512)
)
LANGUAGE SQL
AS $$
INSERT INTO moderators (username, password) VALUES (username_val, password_val);
$$;

--Login Moderator
CREATE OR REPLACE PROCEDURE Login_Moderator(
	IN user_credential varchar(30),
	IN user_password varchar(512),
	INOUT response bool
)
LANGUAGE PLPGSQL
AS $$
BEGIN
	IF EXISTS
	   (SELECT username, password
		FROM moderators
		WHERE username = user_credential
		AND password = user_password)
	THEN
	response := true;
	ELSE
	response := false;
	RAISE EXCEPTION 'FAILED';
	END IF;
END $$;

--Moderator update question topic
CREATE OR REPLACE PROCEDURE Update_Question_Topic(
	IN questionID uuid,
	IN new_title varchar(200)
)
LANGUAGE PLPGSQL
AS $$
BEGIN
	UPDATE questions
	SET title = new_title
	WHERE id = questionID;
END
$$;

--Moderator merge questions
CREATE OR REPLACE procedure merge_questions(remainingq_id uuid, vanishingq_id uuid)
  language plpgsql
as
$$
BEGIN
	UPDATE answers
	SET question_id = remainingQ_ID
	WHERE question_id = vanishingQ_ID;

	DELETE FROM question_best_answers
	WHERE question_id = vanishingq_id;

	DELETE FROM topic_question
	WHERE question_id = vanishingq_id;

	DELETE FROM users_report_questions
	WHERE question_id=vanishingq_id;

	DELETE FROM questions
	WHERE id = vanishingQ_ID;
END
$$;

--Moderator delete questions
CREATE OR REPLACE procedure delete_question_moderator(vanishing_id uuid)
  language plpgsql
as
$$
BEGIN
  DELETE FROM answers
  WHERE question_id = vanishing_id;

  DELETE FROM question_best_answers
  WHERE question_id = vanishing_id;

  DELETE FROM topic_question
  WHERE question_id = vanishing_id;

  DELETE FROM users_report_questions
  WHERE question_id=vanishing_id;

  DELETE FROM questions
  WHERE id = vanishing_ID;
END
$$;

--Moderator delete user
CREATE OR REPLACE procedure delete_user_moderator(vanishing_id uuid)
  language plpgsql
as
$$
BEGIN
  DELETE FROM users
  WHERE id = vanishing_ID;
END
$$;






