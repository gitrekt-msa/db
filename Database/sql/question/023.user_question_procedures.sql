-- Insert Question Bookmark
CREATE OR REPLACE PROCEDURE Insert_User_Bookmark_Question(
  IN user_id_val uuid,
  IN question_id_val uuid
)
LANGUAGE SQL
AS $$
  INSERT INTO user_bookmark_question (user_id, question_id)
    VALUES (user_id_val, question_id_val);
$$;

CREATE OR REPLACE PROCEDURE Delete_User_Bookmark_Question
(
  IN user_id_val UUID,
  IN question_id_val UUID
)
  LANGUAGE SQL
AS $$
DELETE FROM user_bookmark_question WHERE user_id = user_id_val AND question_id = question_id_val;
$$;

-- Insert Question Star
CREATE OR REPLACE PROCEDURE Insert_User_Star_Question(
  IN user_id_val uuid,
  IN question_id_val uuid
)
  LANGUAGE SQL
AS $$
  INSERT INTO user_star_question (user_id, question_id)
    VALUES (user_id_val, question_id_val);
$$;

CREATE OR REPLACE PROCEDURE Delete_User_Star_Question
(
  IN user_id_val UUID,
  IN question_id_val UUID
)
  LANGUAGE SQL
AS $$
DELETE FROM user_star_question WHERE user_id = user_id_val AND question_id = question_id_val;
$$;

-- Insert Question Subscription
CREATE OR REPLACE PROCEDURE Insert_User_Subscription_Question(
  IN user_id_val uuid,
  IN question_id_val uuid
)
  LANGUAGE SQL
AS $$
  INSERT INTO user_subscribe_to_question (user_id, question_id)
    VALUES (user_id_val, question_id_val);
$$;

CREATE OR REPLACE PROCEDURE Delete_User_Subscription_Question
(
  IN user_id_val UUID,
  IN question_id_val UUID
)
  LANGUAGE SQL
AS $$
DELETE FROM user_subscribe_to_question WHERE user_id = user_id_val AND question_id = question_id_val;
$$;

-- Get questions bookmarked by a certain user
CREATE OR REPLACE FUNCTION Get_Bookmarked_Questions(
  IN page_number INT,
  IN user_id_val UUID
) RETURNS TABLE (
  question_id UUID,
  user_id UUID,
  poll_id VARCHAR(20),
  title VARCHAR(200),
  body TEXT,
  upvotes INT,
  subscribers INT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  media JSONB
)
LANGUAGE SQL
AS $$
  SELECT
    questions.id,
    questions.user_id,
    questions.poll_id,
    questions.title,
    questions.body,
    questions.upvotes,
    questions.subscribers,
    questions.created_at,
    questions.updated_at,
    questions.media
  FROM questions
    INNER JOIN user_bookmark_question ON user_bookmark_question.question_id = questions.id
      WHERE user_bookmark_question.user_id = user_id_val AND questions.deleted_at IS NULL
      ORDER BY created_at DESC
      OFFSET 50 * page_number LIMIT 50;
$$;