CREATE TABLE IF NOT EXISTS questions
(
    id UUID PRIMARY KEY,
    user_id UUID REFERENCES users(id) ON DELETE CASCADE,
    poll_id VARCHAR(20),
    title VARCHAR(200) NOT NULL, -- Consider changing data type to TEXT
    body TEXT,
    upvotes INT DEFAULT 0,
    subscribers INT DEFAULT 0,
    created_at TIMESTAMP DEFAULT Now(),
    updated_at TIMESTAMP DEFAULT Now(),
    deleted_at TIMESTAMP,
    media JSONB
);
