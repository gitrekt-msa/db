-- Insert question in questions table
CREATE OR REPLACE PROCEDURE Insert_Question
(
    IN id_val uuid,
    IN user_id_val uuid,
    IN title_val VARCHAR(200),
    IN body_val TEXT,
    IN media_val JSONB
)
AS $$
  BEGIN
	  IF EXISTS(SELECT 1 FROM questions WHERE id = id_val AND user_id = user_id_val) THEN
		  UPDATE questions SET title = title_val, body = body_val, media = media_val WHERE id = id_val;
	  ELSIF EXISTS(SELECT 1 FROM questions WHERE id = id_val) THEN
		  RAISE EXCEPTION 'You do not have the permission to do that';
	  ELSE
		  INSERT INTO questions (id, user_id, title, body, media) VALUES (id_val, user_id_val, title_val, body_val, media_val);
	  END IF;
  END;
$$
LANGUAGE plpgsql;

-- Soft delete a question
CREATE OR REPLACE PROCEDURE Delete_Question
(
    IN id_val uuid,
    IN user_id_val uuid
)
AS $$
BEGIN
	IF EXISTS(SELECT 1 FROM questions WHERE id = id_val AND user_id = user_id_val) THEN
		UPDATE questions SET deleted_at = Now() WHERE id = id_val AND user_id = user_id_val;
	ELSIF EXISTS(SELECT 1 FROM questions WHERE id = id_val) THEN
		RAISE EXCEPTION 'You do not have the permission to do that';
	ELSE
		RAISE EXCEPTION 'Question not found';
	END IF;
END;
$$
LANGUAGE plpgsql;

-- Return a question with a specified ID
CREATE OR REPLACE FUNCTION Get_Question_By_Id
(
  IN id_val UUID
) RETURNS TABLE (
  question_id UUID,
  user_id UUID,
  poll_id VARCHAR(20),
  title VARCHAR(200),
  body TEXT,
  upvotes INT,
  subscribers INT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  deleted_at TIMESTAMP,
  media JSONB
)
LANGUAGE SQL
AS $$
  SELECT
    id,
    user_id,
    poll_id,
    title,
    body,
    upvotes,
    subscribers,
    created_at,
    updated_at,
    deleted_at,
    media
  FROM questions
    WHERE id = id_val AND deleted_at IS NULL;
$$;

-- Get all questions (paged)
CREATE OR REPLACE FUNCTION Get_all_questions
(
  IN page_number INT
) RETURNS TABLE (
  question_id UUID,
  user_id UUID,
  poll_id VARCHAR(20),
  title VARCHAR(200),
  body TEXT,
  upvotes INT,
  subscribers INT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  media JSONB
)
LANGUAGE SQL
AS $$
   SELECT
    id,
    user_id,
    poll_id,
    title,
    body,
    upvotes,
    subscribers,
    created_at,
    updated_at,
    media
  FROM questions
    WHERE deleted_at IS NULL
    ORDER BY created_at DESC
    OFFSET 50 * page_number LIMIT 50;
$$;

-- Add poll id to question
CREATE OR REPLACE PROCEDURE Add_Poll_Question(
    IN question_id uuid,
    IN poll_id_val VARCHAR(50)
)
LANGUAGE SQL
AS $$ 
  UPDATE questions
  SET poll_id = poll_id_val
  WHERE id = question_id;
$$;

-- Clear poll id for a question
CREATE OR REPLACE PROCEDURE Clear_Poll_Question(
    IN question_id uuid
)
LANGUAGE SQL
AS $$ 
  UPDATE questions
  SET poll_id = NULL
  WHERE id = question_id;
$$;
