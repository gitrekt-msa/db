--Create Bookmarks table
CREATE TABLE IF NOT EXISTS user_bookmark_question(
  user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  question_id uuid REFERENCES questions(id) ON DELETE CASCADE,
  PRIMARY KEY (user_id,question_id)
);

--Create Star table
CREATE TABLE IF NOT EXISTS user_star_question(
  user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  question_id uuid REFERENCES questions(id) ON DELETE CASCADE,
  PRIMARY KEY (user_id,question_id)
);

--Create Subscriptions table
CREATE TABLE IF NOT EXISTS user_subscribe_to_question(
  user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  question_id uuid REFERENCES questions(id) ON DELETE CASCADE,
  PRIMARY KEY (user_id,question_id)
);
