create or replace procedure insert_best_answer(question_id_val uuid, answer_id_val uuid, uuid_val uuid)
  language plpgsql
as
$$
BEGIN
  IF EXISTS(SELECT 1 FROM questions WHERE id = question_id_val AND user_id = uuid_val) THEN
    IF EXISTS(SELECT 1 FROM question_best_answers WHERE question_id = question_id_val AND answer_id = answer_id_val) THEN
      RAISE EXCEPTION 'You do not have the permission to do that';
    ELSEIF EXISTS(SELECT 1 FROM question_best_answers WHERE question_id = question_id_val) THEN
      IF EXISTS(SELECT 1 FROM answers WHERE question_id = question_id_val AND id = answer_id_val) THEN
        UPDATE question_best_answers SET answer_id = answer_id_val WHERE question_id = question_id_val;
      ELSE
        RAISE EXCEPTION 'You do not have the permission to do that';
      END IF;
    ELSE
      IF EXISTS(SELECT 1 FROM answers WHERE question_id = question_id_val AND id = answer_id_val) THEN
        INSERT INTO question_best_answers (question_id, answer_id)	VALUES (question_id_val, answer_id_val);
      ELSE
        RAISE EXCEPTION 'You do not have the permission to do that';
      END IF;
    END IF;
  ELSE
    RAISE EXCEPTION 'You do not have the permission to do that';
  END IF;
END;
$$;


create or replace procedure delete_best_answer(ques_id uuid, uuid_val uuid)
  language plpgsql
as
$$
BEGIN
  IF EXISTS(SELECT 1 FROM questions WHERE id = ques_id AND user_id = uuid_val) THEN
    IF EXISTS(SELECT 1 FROM question_best_answers WHERE question_id = ques_id) THEN
      DELETE FROM question_best_answers WHERE question_id = ques_id;
    ELSE
      RAISE EXCEPTION 'You do not have the permission to do that';
    END IF;
  ELSE
    RAISE EXCEPTION 'You do not have the permission to do that';
  END IF;
END;
$$;