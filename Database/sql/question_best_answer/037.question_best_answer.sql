CREATE TABLE IF NOT EXISTS question_best_answers
(
    question_id uuid PRIMARY KEY REFERENCES questions(id),
    answer_id uuid REFERENCES answers(id) on delete cascade
);