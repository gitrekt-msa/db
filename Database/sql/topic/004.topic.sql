CREATE TABLE IF NOT EXISTS topics
(
  id uuid PRIMARY KEY,
	name VARCHAR(200) UNIQUE NOT NULL,
	subscribers INT DEFAULT 0
);
