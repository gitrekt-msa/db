CREATE OR REPLACE PROCEDURE Insert_Topic
(
    IN id_val uuid,
    IN name_val VARCHAR(200)
)
LANGUAGE SQL
AS $$ 
INSERT INTO topics (id, name) VALUES (id_val, name_val);
$$;