-- Insert Answer Report
CREATE OR REPLACE PROCEDURE Insert_Answer_Report(
  IN user_id_val uuid,
 	IN answer_id_val uuid,
 	IN report_text_val text
)
LANGUAGE SQL
AS $$
  INSERT INTO answers_reports (user_id, answer_id, report_text) VALUES (user_id_val, answer_id_val, report_text_val);
$$;
