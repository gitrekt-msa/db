-- CREATE ANSWERS REPORTS TABLE(s)
CREATE TABLE IF NOT EXISTS answers_reports(
  user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  answer_id uuid REFERENCES answers(id) ON DELETE CASCADE,
  report_text  text,
  PRIMARY KEY (user_id,answer_id)
);
