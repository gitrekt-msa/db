
CREATE EXTENSION IF NOT EXISTS pg_trgm;
	
CREATE OR REPLACE FUNCTION get_question_by_title (
  IN title_in varchar(100)
)
  RETURNS TABLE (
                  id uuid,
                  user_id uuid,
                  title varchar(200),
                  body text,
                  upvotes integer,
                  subscribers integer
                ) AS
$BODY$
SELECT public.questions.id, public.questions.user_id, public.questions.title, public.questions.body, public.questions.upvotes, public.questions.subscribers
FROM public.questions WHERE similarity(public.questions.title, title_in)>0.4 and public.questions.deleted_at is null
$BODY$
  LANGUAGE sql;

CREATE OR REPLACE FUNCTION get_question_by_topic (
  IN topic varchar(100)
)
  RETURNS TABLE (
                  id uuid,
                  user_id uuid,
                  title varchar(200),
                  body text,
                  upvotes integer,
                  subscribers integer
                ) AS
$BODY$
SELECT public.questions.id, public.questions.user_id, public.questions.title,
       public.questions.body, public.questions.upvotes, public.questions.subscribers
FROM public.questions inner join topic_question tq on questions.id = tq.question_id
                      INNER JOIN topics t on tq.topic_id = t.id
WHERE similarity(t.name, topic) > 0.4 and public.questions.deleted_at is null;;
$BODY$
  LANGUAGE sql;

