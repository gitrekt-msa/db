CREATE EXTENSION IF NOT EXISTS pg_trgm;


CREATE OR REPLACE FUNCTION get_user_by_email (
  IN email_in varchar(50)
)
    RETURNS TABLE (
                  id uuid,
                  email varchar(50),
                  username varchar(40),
                  first_name varchar(30),
                  last_name varchar(30)
                ) AS
$BODY$
SELECT public.users.id, public.users.email, public.users.username, public.users.first_name, public.users.last_name
FROM public.users WHERE similarity(public.users.email, email_in) > 0.4
$BODY$
  LANGUAGE sql;
CREATE OR REPLACE FUNCTION get_user_by_username(
  IN username_in varchar(50)
)
  RETURNS TABLE (
                  id uuid,
                  email varchar(50),
                  username varchar(40),
                  first_name varchar(30),
                  last_name varchar(30)
                ) AS
$BODY$
SELECT public.users.id, public.users.email, public.users.username, public.users.first_name, public.users.last_name
FROM public.users WHERE similarity(public.users.username, username_in) > 0.4
$BODY$
  LANGUAGE sql;

select * from users
where  similarity('hady mohamed', CONCAT (first_name,' ', last_name))  > 0.4;

CREATE OR REPLACE FUNCTION get_user_by_name (
  IN name_in varchar(50)
)
  RETURNS TABLE (
                  id uuid,
                  email varchar(50),
                  username varchar(40),
                  first_name varchar(30),
                  last_name varchar(30)
                ) AS
$BODY$
SELECT public.users.id, public.users.email, public.users.username, public.users.first_name, public.users.last_name
FROM public.users WHERE similarity(concat(public.users.first_name, ' ', public.users.last_name), name_in) > 0.3
$BODY$
  LANGUAGE sql;




