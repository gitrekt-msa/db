-- Create user subscribe to discussion relation table
CREATE TABLE IF NOT EXISTS user_subscribe_discussion(
    user_id uuid REFERENCES users(id) ON DELETE CASCADE,
    discussion_id uuid REFERENCES discussions(id),
    PRIMARY KEY (user_id, discussion_id)
);
