-- Insert user subscribe discussion relation
CREATE OR REPLACE PROCEDURE Insert_User_Subscribe_Discussion(
  IN user_id_val uuid,
  IN discussion_id_val uuid
)
LANGUAGE SQL
AS $$
  INSERT INTO user_subscribe_discussion (user_id, discussion_id)
  VALUES (user_id_val, discussion_id_val);
$$;

-- Delete user subscribe discussion
CREATE OR REPLACE PROCEDURE Delete_User_Subscribe_Discussion
(
    IN user_id_val uuid,
    IN discussion_id_val uuid
)
LANGUAGE SQL
AS $$
    DELETE FROM user_subscribe_discussion WHERE user_id = user_id_val AND discussion_id = discussion_id_val;
$$;