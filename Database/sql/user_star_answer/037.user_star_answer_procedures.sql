-- Insert STAR
CREATE OR REPLACE PROCEDURE Insert_Answer_Star(
  IN user_id_val uuid,
 	IN answer_id_val uuid
)
LANGUAGE SQL
AS $$
  INSERT INTO answers_stars (user_id, answer_id) VALUES (user_id_val, answer_id_val);

  UPDATE answers
  SET stars = stars + 1
  WHERE id = answer_id_val
$$;

-- Delete STAR
CREATE OR REPLACE PROCEDURE Delete_Answer_Star(
  IN user_id_val uuid,
 	IN answer_id_val uuid
)
LANGUAGE SQL
AS $$
  DELETE FROM answers_stars
  WHERE user_id = user_id_val AND answer_id = answer_id_val
$$;
