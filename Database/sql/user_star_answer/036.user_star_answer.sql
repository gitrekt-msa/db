-- CREATE USER STARS ANSWERS TABLE(s)
CREATE TABLE IF NOT EXISTS answers_stars(
  user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  answer_id  uuid REFERENCES answers(id)  on delete cascade,
  PRIMARY KEY (user_id, answer_id)
);
