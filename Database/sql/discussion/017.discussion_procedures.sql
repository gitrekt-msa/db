-- Insert (or update) Discussion
CREATE OR REPLACE PROCEDURE Insert_Discussion(
  IN id_val UUID,
  IN title_val VARCHAR(100),
  IN body_val VARCHAR(500),
  IN is_public_val BOOLEAN,
  IN topic_id_val UUID,
  IN user_id_val UUID,
  IN media_val JSONB
)
AS $$
  BEGIN
    IF EXISTS(SELECT 1 FROM discussions WHERE id = id_val AND user_id = user_id_val) THEN
      UPDATE discussions SET title = title_val, body = body_val, is_public = is_public_val, topic_id = topic_id_val, media = media_val WHERE id = id_val;
    ELSEIF EXISTS(SELECT 1 FROM discussionS WHERE id = id_val) THEN
		  RAISE EXCEPTION 'You do not have the permission to do that';
    ELSE
      INSERT INTO discussions (id, title, body, is_public, topic_id, user_id, media) 
        VALUES (id_val, title_val, body_val, is_public_val, topic_id_val, user_id_val, media_val);
    END IF;
  END;
$$
LANGUAGE plpgsql;

-- Get discussion by id
CREATE OR REPLACE FUNCTION Get_Discussion_By_Id
(
  IN id_val UUID
) RETURNS TABLE (
  discussion_id UUID,
  title VARCHAR(100),
  body VARCHAR(500),
  subscribers_count INT,
  is_public BOOLEAN,
  poll_id VARCHAR(20),
  topic_id UUID,
  user_id UUID,
  created_at TIMESTAMP,
  media JSONB
) LANGUAGE SQL
AS $$
  SELECT
    id,
    title,
    body,
    subscribers_count,
    is_public,
    poll_id,
    topic_id,
    user_id,
    created_at,
    media
  FROM discussions
    WHERE id = id_val AND deleted_at IS NULL;
$$;

-- Delete discussion
CREATE OR REPLACE PROCEDURE Delete_Discussion
(
    IN id_val uuid,
    IN user_id_val uuid
)
AS $$
  BEGIN
    IF EXISTS(SELECT 1 FROM discussions WHERE id = id_val AND user_id = user_id_val) THEN
      UPDATE discussions SET deleted_at = Now() WHERE id = id_val AND user_id = user_id_val;
    ELSIF EXISTS(SELECT 1 FROM discussions WHERE id = id_val) THEN
      RAISE EXCEPTION 'You do not have the permission to do that';
    ELSE
      RAISE EXCEPTION 'Discussion not found';
    END IF;
  END;
$$
LANGUAGE plpgsql;

-- Add poll id to discussion
CREATE OR REPLACE PROCEDURE Add_Poll_Discussion(
    IN discussion_id uuid,
    IN poll_id_val VARCHAR(20)
)
LANGUAGE SQL
AS $$ 
  UPDATE discussions
  SET poll_id = poll_id_val
  WHERE id = discussion_id
$$;

-- Clear poll id for a  discussion
CREATE OR REPLACE PROCEDURE Clear_Poll_Discussion(
    IN discussion_id uuid
)
LANGUAGE SQL
AS $$ 
  UPDATE discussions
  SET poll_id = NULL
  WHERE id = discussion_id
$$;
