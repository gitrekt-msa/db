-- CREATE DISCUSSION TABLE(s)
CREATE TABLE IF NOT EXISTS discussions(
  id UUID PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  body VARCHAR(500) NOT NULL,
  subscribers_count INT DEFAULT 0,
  is_public BOOLEAN DEFAULT TRUE,
  poll_id VARCHAR(20),
  topic_id UUID REFERENCES topics(id) NOT NULL,
  user_id UUID REFERENCES users(id) ON DELETE CASCADE,
  created_at TIMESTAMP DEFAULT now(),
  deleted_at TIMESTAMP,
  media JSONB
);
