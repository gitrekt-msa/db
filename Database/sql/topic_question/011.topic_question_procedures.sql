CREATE OR REPLACE PROCEDURE Insert_Question_Topic
(
  IN topic_id_val UUID,
  IN question_id_val UUID
)
LANGUAGE SQL
AS $$
  INSERT INTO topic_question (topic_id, question_id) VALUES (topic_id_val, question_id_val);
$$;

CREATE OR REPLACE PROCEDURE Delete_Question_Topic
(
  IN topic_id_val UUID,
  IN question_id_val UUID
)
LANGUAGE SQL
AS $$
  DELETE FROM topic_question WHERE topic_id = topic_id_val AND question_id = question_id_val;
$$;


CREATE OR REPLACE FUNCTION Get_Questions_By_Topic(
  IN page_number INT,
  IN topic_id_val UUID
) RETURNS TABLE (
  question_id UUID,
  user_id UUID,
  poll_id VARCHAR(20),
  title VARCHAR(200),
  body TEXT,
  upvotes INT,
  subscribers INT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  media JSONB
)
LANGUAGE SQL
AS $$
  SELECT
    questions.id,
    questions.user_id,
    questions.poll_id,
    questions.title,
    questions.body,
    questions.upvotes,
    questions.subscribers,
    questions.created_at,
    questions.updated_at,
    questions.media
  FROM questions
	  INNER JOIN topic_question ON topic_question.question_id = questions.id
      WHERE topic_question.topic_id = topic_id_val AND questions.deleted_at IS NULL
      ORDER BY created_at DESC
      OFFSET 50 * page_number LIMIT 50;
$$;