CREATE TABLE IF NOT EXISTS topic_question
(
	topic_id uuid REFERENCES topics(id) NOT NULL,
  question_id uuid REFERENCES questions(id) ON DELETE CASCADE,
	PRIMARY KEY(topic_id, question_id)
);
