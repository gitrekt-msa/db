CREATE TABLE IF NOT EXISTS answers
(
  id uuid PRIMARY KEY,
  question_id uuid REFERENCES questions(id) ON DELETE CASCADE,
  discussion_id uuid REFERENCES discussions(id),
  user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  answer_text TEXT,
  stars INT DEFAULT 0,
  is_public BOOLEAN,
  media JSONB,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  deleted_at TIMESTAMP,
  CHECK (
    (
      (question_id IS NOT NULL)::INTEGER +
      (discussion_id IS NOT NULL)::INTEGER
    ) = 1
  )
);

CREATE index ON answers (question_id) WHERE question_id IS NOT NULL;
CREATE index ON answers (discussion_id) WHERE discussion_id IS NOT NULL;
