-- Insert Answer
CREATE OR REPLACE PROCEDURE Insert_Answer(
  IN id_val uuid,
  IN question_id_val uuid,
  IN discussion_id_val uuid,
  IN user_id_val uuid,
  IN answer_text_val TEXT,
  IN stars_val INT,
  IN is_public_val BOOLEAN,
  IN media_val JSONB
)
  LANGUAGE plpgsql
AS $$


BEGIN
  IF EXISTS(SELECT 1 FROM answers WHERE id = id_val AND user_id = user_id_val AND deleted_at IS NOT NULL) THEN
    RAISE EXCEPTION 'You can not update a deleted answer';

  ELSEIF EXISTS(SELECT 1 FROM answers WHERE id = id_val AND user_id = user_id_val) THEN
    UPDATE answers
    SET updated_at = NOW(),
        answer_text = answer_text_val,
        media = COALESCE(NULLIF(media_val, null), media_val),

        is_public = COALESCE(NULLIF(is_public_val, null), is_public_val)

    WHERE id = id_val AND user_id = user_id_val;
  ELSIF EXISTS(SELECT 1 FROM answers WHERE id = id_val) THEN
    RAISE EXCEPTION 'You do not have the permission to do that';
  ELSE
    INSERT INTO answers (id, question_id, discussion_id, user_id, answer_text, stars, is_public, media,  created_at, updated_at)
    VALUES (id_val, question_id_val, discussion_id_val, user_id_val, answer_text_val, stars_val, is_public_val, media_val, NOW(), NOW());
  END IF;
END;

$$;

-- Get User Password
CREATE OR REPLACE FUNCTION Get_Answer_ID(
  IN id_val uuid
) RETURNS TABLE(
                 id uuid ,
                 question_id uuid,
                 discussion_id uuid ,
                 user_id uuid ,
                 answer_text TEXT,
                 stars INT ,
                 is_public BOOLEAN,
                 media JSONB,
                 created_at TIMESTAMP,
                 updated_at TIMESTAMP,
                 deleted_at TIMESTAMP
               )
AS $$ SELECT * FROM answers WHERE id = id_val $$ LANGUAGE SQL;



CREATE OR REPLACE PROCEDURE Delete_Answer(
  id_val uuid,
  user_id_val uuid
)

  LANGUAGE plpgsql
AS $$
BEGIN
  IF EXISTS(SELECT 1 FROM answers WHERE id = id_val AND user_id = user_id_val) THEN
    UPDATE answers SET deleted_at = Now() WHERE id = id_val AND user_id = user_id_val;
  ELSIF EXISTS(SELECT 1 FROM answers WHERE id = id_val) THEN
    RAISE EXCEPTION 'You do not have the permission to do that';
  ELSE
    RAISE EXCEPTION 'Answer not found';
  END IF;

END;
$$;