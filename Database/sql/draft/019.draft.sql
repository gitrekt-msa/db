CREATE TABLE IF NOT EXISTS drafts
(
    user_id uuid PRIMARY KEY REFERENCES users(id) ON DELETE CASCADE,
    title VARCHAR(200),
    body TEXT
);
