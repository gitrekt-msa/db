CREATE OR REPLACE PROCEDURE Insert_Draft
(
    IN user_id_val uuid,
    IN title_val VARCHAR(200),
    IN body_val TEXT
)
LANGUAGE SQL
AS $$ 
    INSERT INTO drafts (user_id, title, body)
        VALUES (user_id_val, title_val, body_val)
        ON CONFLICT (user_id) DO UPDATE SET title = title_val, body = body_val;
$$;

CREATE OR REPLACE PROCEDURE Delete_Draft
(
    IN user_id_val uuid
)
LANGUAGE SQL
AS $$
    DELETE FROM drafts WHERE user_id = user_id_val;
$$;

-- Return a draft with a specified user ID
CREATE OR REPLACE FUNCTION Get_Draft_By_User_Id
(
  IN user_id_val UUID
) RETURNS TABLE (
  user_id UUID,
  title VARCHAR(200),
  body TEXT
)
LANGUAGE SQL
AS $$
  SELECT
    user_id,
    title,
    body
  FROM drafts WHERE user_id = user_id_val;
$$;