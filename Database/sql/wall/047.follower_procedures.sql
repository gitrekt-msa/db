-- Insert Follower
CREATE OR REPLACE PROCEDURE Insert_follower(
  IN usr_id uuid,
 	IN foll_id uuid
)
LANGUAGE SQL
AS $$
  INSERT INTO followers (user_id, follower_Id)
  VALUES (usr_id, foll_id);
$$;


-- Delete Follower
CREATE OR REPLACE PROCEDURE Delete_follower(
  IN usr_id uuid,
 	IN foll_id uuid
)
LANGUAGE SQL
AS $$
  DELETE FROM followers WHERE user_id = usr_id AND follower_id = foll_id;
$$;

-- Get User Follow State
CREATE OR REPLACE FUNCTION Get_Follow_State(
    IN follower_user_id_val uuid,
    IN followed_user_id_val uuid)
    RETURNS TABLE(user_id uuid)
AS $$
SELECT user_id FROM followers WHERE user_id = follower_user_id_val AND  follower_id = followed_user_id_val;
$$ LANGUAGE SQL;