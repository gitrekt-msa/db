CREATE OR REPLACE FUNCTION View_Followers(
  IN userId uuid
)
  RETURNS TABLE (
    followers uuid
                ) AS
$BODY$
SELECT public.followers.follower_id
FROM public.followers
WHERE userId = followers.user_id
$BODY$
  LANGUAGE sql;

CREATE OR REPLACE FUNCTION View_Questions (
  IN userId uuid
)
  RETURNS TABLE (
                  questionId uuid,
                  userId uuid,
                  firstName varchar(30),
                  lastName varchar(30),
                  title varchar(200),
                  body TEXT,
                  upvotes INT,
                  subscribers INT
                ) AS
$BODY$
SELECT public.questions.id,public.questions.user_id,public.users.first_name,public.users.last_name,
       public.questions.title,public.questions.body,public.questions.upvotes,public.questions.subscribers
FROM public.questions inner join View_Followers($1) as userFollowers
                                 on public.questions.user_id = userFollowers.followers
      left join public.users
          on public.questions.user_id = public.users.id
where questions.user_id = userFollowers.followers
$BODY$
  LANGUAGE sql;

CREATE OR REPLACE FUNCTION View_Answers (
  IN userId uuid
)
  RETURNS TABLE (
                  questionId uuid,
                  userId uuid,
                  firstName varchar(30),
                  lastName varchar(30),
                  title varchar(200),
                  questionText TEXT,
                  upvotes INT,
                  subscribers INT,
                  answerId uuid,
                  answerUserId uuid,
                  answerFirstName varchar(30),
                  answerLastName varchar(30),
                  answerText TEXT
                ) AS
$BODY$
SELECT public.questions.id,public.questions.user_id,questionUsers.first_name,questionUsers.last_name,
       public.questions.title,public.questions.body,public.questions.upvotes,public.questions.subscribers,
       public.answers.id,public.answers.user_id,answerUsers.first_name,answerUsers.last_name,public.answers.answer_text
  FROM public.answers inner join View_Followers($1) as userFollowers
         on public.answers.user_id = userFollowers.followers
        left join public.users as answerUsers
         on public.answers.user_id = answerUsers.id
        left join public.questions
         on public.answers.question_id = public.questions.id
        left join public.users as questionUsers
         on public.questions.user_id = questionUsers.id
    where public.questions.id is not null
$BODY$
  LANGUAGE sql;
