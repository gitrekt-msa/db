-- CREATE USER_FOLLOWERS TABLE(s)
CREATE TABLE IF NOT EXISTS followers(
  user_Id uuid ,
  follower_Id uuid,
  PRIMARY KEY (user_Id, follower_Id)
);
