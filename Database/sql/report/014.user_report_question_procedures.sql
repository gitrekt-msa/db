CREATE OR REPLACE PROCEDURE Insert_Report
(
    IN user_id_val uuid,
    IN question_id_val uuid
)
LANGUAGE SQL
AS $$ 
    INSERT INTO users_report_questions (user_id, question_id)
        VALUES (user_id_val, question_id_val);
$$;