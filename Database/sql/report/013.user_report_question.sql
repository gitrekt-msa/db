CREATE TABLE IF NOT EXISTS users_report_questions
(
    user_id uuid REFERENCES users(id) ON DELETE CASCADE,
    question_id uuid REFERENCES questions(id) NOT NULL,
    PRIMARY KEY(user_id, question_id)
);
