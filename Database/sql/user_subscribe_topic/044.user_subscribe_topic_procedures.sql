-- Insert user subscribe topic relation
CREATE OR REPLACE PROCEDURE Insert_User_Subscribe_Topic
(
  IN user_id_val UUID,
  IN topic_id_val UUID
)
LANGUAGE SQL
AS $$
  INSERT INTO user_subscribe_topic (user_id, topic_id) 
    VALUES (user_id_val, topic_id_val);
$$;

CREATE OR REPLACE PROCEDURE Delete_User_Subscribe_Topic
(
  IN user_id_val UUID,
  IN topic_id_val UUID
)
LANGUAGE SQL
AS $$
  DELETE FROM user_subscribe_topic
    WHERE user_id = user_id_val AND topic_id = topic_id_val;
$$;