-- Create user subscribe topic relation table
CREATE TABLE IF NOT EXISTS user_subscribe_topic(
    user_id uuid REFERENCES users(id) ON DELETE CASCADE,
    topic_id uuid REFERENCES topics(id),
    PRIMARY KEY (user_id, topic_id)
);
