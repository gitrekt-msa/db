-- Users seed
CALL Insert_User(
  '46eb6050-2aa8-434d-b65b-05b4eeccb1d5',
  'user1@balabizo.com',
  'user-1',
  '7FB65D38DA9B96B52FEEE28FE1406364',
  'User',
  '1'
);


CALL Insert_User(
    'aaceaf3a-5ceb-11e9-8647-d663bd873d93',
    'user2@balabizo.com',
    'user-2',
    '7FB65D38DA9B96B52FEEE28FE1406364',
    'User',
    '2'
);

CALL Insert_User(
    'aaceb61a-5ceb-11e9-8647-d663bd873d93',
    'user3@balabizo.com',
    'user-3',
    '7FB65D38DA9B96B52FEEE28FE1406364',
    'User',
    '3'
);

CALL Insert_User(
    'aacebbb0-5ceb-11e9-8647-d663bd873d93',
    'user4@balabizo.com',
    'user-4',
    '7FB65D38DA9B96B52FEEE28FE1406364',
    'User',
    '4'
);

CALL Insert_User(
    '44fde6c8-5cf8-11e9-8647-d663bd873d93',
    'user5@balabizo.com',
    'user-5',
    '7FB65D38DA9B96B52FEEE28FE1406364',
    'User',
    '5'
);

-- Topics seed
CALL Insert_Topic(
    'aacebd04-5ceb-11e9-8647-d663bd873d93',
    'topic1'
);

CALL Insert_topic(
    'aacebe30-5ceb-11e9-8647-d663bd873d93',
    'topic2'
);

CALL Insert_Topic(
    'aacebf5c-5ceb-11e9-8647-d663bd873d93',
    'topic3'
);

CALL Insert_Topic(
    'aacec07e-5ceb-11e9-8647-d663bd873d93',
    'topic4'
);

  CALL Insert_Topic(
  '44fde97a-5cf8-11e9-8647-d663bd873d93',
  'topic5'
);

-- User subscribe topic seed
CALL Insert_User_Subscribe_Topic(
    '46eb6050-2aa8-434d-b65b-05b4eeccb1d5',
    'aacebd04-5ceb-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
    '46eb6050-2aa8-434d-b65b-05b4eeccb1d5',
    'aacebe30-5ceb-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
    '46eb6050-2aa8-434d-b65b-05b4eeccb1d5',
    'aacebf5c-5ceb-11e9-8647-d663bd873d93'
);


CALL Insert_User_Subscribe_Topic(
    'aaceaf3a-5ceb-11e9-8647-d663bd873d93',
    'aacebd04-5ceb-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
    'aaceaf3a-5ceb-11e9-8647-d663bd873d93',
    'aacebe30-5ceb-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
    'aaceb61a-5ceb-11e9-8647-d663bd873d93',
    'aacebd04-5ceb-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
    'aacebbb0-5ceb-11e9-8647-d663bd873d93',
    'aacebd04-5ceb-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
    'aacebbb0-5ceb-11e9-8647-d663bd873d93',
    'aacec07e-5ceb-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
'aacebbb0-5ceb-11e9-8647-d663bd873d93',
'44fde97a-5cf8-11e9-8647-d663bd873d93'
);

CALL Insert_User_Subscribe_Topic(
    '44fde6c8-5cf8-11e9-8647-d663bd873d93',
    '44fde97a-5cf8-11e9-8647-d663bd873d93'
);

-- Followers seed
CALL Insert_follower(
    '44fde6c8-5cf8-11e9-8647-d663bd873d93',
    '46eb6050-2aa8-434d-b65b-05b4eeccb1d5'
);

CALL Insert_follower(
    'aacebbb0-5ceb-11e9-8647-d663bd873d93',
    '46eb6050-2aa8-434d-b65b-05b4eeccb1d5'
);

-- Questions seed
CALL Insert_Question(
    '3d458d78-5d07-11e9-8647-d663bd873d93',
    '46eb6050-2aa8-434d-b65b-05b4eeccb1d5',
    'q1_title',
    'q1_body',
    '{"paths":[]}'
);

CALL Insert_Question(
    '3d4590a2-5d07-11e9-8647-d663bd873d93',
    'aaceaf3a-5ceb-11e9-8647-d663bd873d93',
    'q2_title',
    'q2_body',
    '{"paths":[]}'
);

CALL Insert_Question(
    '3d459598-5d07-11e9-8647-d663bd873d93',
    'aaceaf3a-5ceb-11e9-8647-d663bd873d93',
    'q3_title',
    'q3_body',
    '{"paths":[]}'
);

CALL Insert_Question(
    '3d4596ec-5d07-11e9-8647-d663bd873d93',
    'aaceaf3a-5ceb-11e9-8647-d663bd873d93',
    'q4_title',
    'q4_body',
    '{"paths":[]}'
);

CALL Insert_Question(
    '3d45982c-5d07-11e9-8647-d663bd873d93',
    'aaceb61a-5ceb-11e9-8647-d663bd873d93',
    'q5_title',
    'q5_body',
    '{"paths":[]}'
);

CALL Insert_Question(
    '71dcfe74-5d0a-11e9-8647-d663bd873d93',
    'aaceb61a-5ceb-11e9-8647-d663bd873d93',
    'q6_title',
    'q6_body',
    '{"paths":[]}'
);

CALL Delete_Question(
    '71dcfe74-5d0a-11e9-8647-d663bd873d93',
    'aaceb61a-5ceb-11e9-8647-d663bd873d93'
);

-- Questions topics
CALL Insert_Question_Topic(
    'aacebd04-5ceb-11e9-8647-d663bd873d93',
    '3d458d78-5d07-11e9-8647-d663bd873d93'
);

CALL Insert_Question_Topic(
    'aacebd04-5ceb-11e9-8647-d663bd873d93',
    '3d4590a2-5d07-11e9-8647-d663bd873d93'
);

CALL Insert_Question_Topic(
    'aacebd04-5ceb-11e9-8647-d663bd873d93',
    '3d459598-5d07-11e9-8647-d663bd873d93'
);

CALL Insert_Question_Topic(
    'aacec07e-5ceb-11e9-8647-d663bd873d93',
    '3d4596ec-5d07-11e9-8647-d663bd873d93'
);

CALL Insert_Question_Topic(
    'aacebd04-5ceb-11e9-8647-d663bd873d93',
    '3d45982c-5d07-11e9-8647-d663bd873d93'
);

CALL Insert_Question_Topic(
    'aacebd04-5ceb-11e9-8647-d663bd873d93',
    '71dcfe74-5d0a-11e9-8647-d663bd873d93'
);

-- Answers seed
CALL Insert_Answer(
    '71dd0144-5d0a-11e9-8647-d663bd873d93',
    '3d4590a2-5d07-11e9-8647-d663bd873d93',
    NULL,
    'aaceaf3a-5ceb-11e9-8647-d663bd873d93',
    'answer1',
    0,
    TRUE,
    '{"paths":[]}'
);

