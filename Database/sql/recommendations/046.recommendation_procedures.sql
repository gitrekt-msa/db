-- Get recommended questions to answer
CREATE OR REPLACE FUNCTION Get_Recommended_Questions 
(
    IN user_id_val UUID
) RETURNS TABLE (
        question_id UUID,
        question_title VARCHAR(200),
        body TEXT, 
        upvotes INT,
        topic_id UUID,
        topic_name VARCHAR(200)
    )
AS $$
    -- Get Topics that a user is subscribed to
    WITH user_topics AS (
        SELECT topic_id 
        FROM user_subscribe_topic 
        WHERE user_id = user_id_val
    ), 
    -- Get Questions in topics that user follows
    topics_questions AS (
        SELECT topic_question.question_id, topic_question.topic_id
        FROM topic_question
        INNER JOIN user_topics ON topic_question.topic_id = user_topics.topic_id
    ),
    -- Get unanswered questions 
    unanswered_questions AS (
        SELECT 
            topics_questions.topic_id,
            topics_questions.question_id
        FROM topics_questions
        WHERE topics_questions.question_id NOT IN (
            SELECT question_id
            FROM answers
            WHERE deleted_at IS NULL AND question_id IS NOT NULL
        )
    )

    -- Return result of question and topic details
    SELECT 
        questions.id, 
        questions.title,
        questions.body,
        questions.upvotes,
        topics.id,
        topics.name
    FROM questions
    INNER JOIN unanswered_questions ON questions.id = unanswered_questions.question_id
    INNER JOIN topics ON topics.id = unanswered_questions.topic_id
    WHERE questions.user_id != user_id_val AND questions.deleted_at IS NULL
    ORDER BY questions.updated_at DESC, questions.upvotes DESC
    LIMIT 5;
$$
LANGUAGE SQL;


-- Get trending questions in a certain topic 
CREATE OR REPLACE FUNCTION Get_Trending_Questions
(
    IN topic_id_val UUID
) RETURNS TABLE (
        question_id UUID,
        question_title VARCHAR(200),
        body TEXT, 
        upvotes INT
    )
AS $$
    SELECT
        questions.id, 
        questions.title,
        questions.body,
        questions.upvotes
    FROM questions
    INNER JOIN topic_question ON topic_question.question_id = questions.id
    WHERE topic_question.topic_id = topic_id_val AND questions.deleted_at IS NULL
    ORDER BY questions.updated_at DESC, questions.upvotes DESC
    LIMIT 5;
$$
LANGUAGE SQL;

-- Recommend users for a user to follow
CREATE OR REPLACE FUNCTION Get_Recommended_Users 
(
    IN user_id_val UUID
) RETURNS TABLE (
    user_id UUID, 
    first_name varchar(30),
    last_name varchar(30)
)
AS $$
    -- Recommended users having 50% of common topics
    WITH recommended_users AS (
        SELECT user_id
        FROM user_subscribe_topic
        WHERE user_id <> user_id_val AND topic_id IN (
            SELECT topic_id 
            FROM user_subscribe_topic 
            WHERE user_id = user_id_val
        )
        GROUP BY user_id
        HAVING COUNT(topic_id) > 0.5 * (
            SELECT COUNT(*)
            FROM user_subscribe_topic
            WHERE user_id = user_id_val
        )
        ORDER BY COUNT(*) DESC
        LIMIT 5
    )

    SELECT id, first_name, last_name
    FROM users INNER JOIN recommended_users ON users.id = recommended_users.user_id;
$$
LANGUAGE SQL; 


-- Recommend topics for user to follow
CREATE OR REPLACE FUNCTION Get_Recommended_Topics
(
    IN user_id_val UUID
) RETURNS TABLE (
    topic_id UUID,
    topic_name VARCHAR(200)
) AS $$
    -- Get followed users
    WITH followed_users AS (
        SELECT user_Id
        FROM followers
        WHERE follower_Id = user_id_val
    )

    SELECT user_subscribe_topic.topic_id, topics.name
    FROM user_subscribe_topic 
    INNER JOIN followed_users
    ON user_subscribe_topic.user_id = followed_users.user_Id
    INNER JOIN topics
    ON topics.id = user_subscribe_topic.topic_id
    WHERE topic_id NOT IN (
        SELECT topic_id 
        FROM user_subscribe_topic 
        WHERE user_id = user_id_val
    )
    GROUP BY user_subscribe_topic.topic_id, topics.name
    ORDER BY COUNT(user_subscribe_topic.user_id) DESC
    LIMIT 5
$$
LANGUAGE SQL;
