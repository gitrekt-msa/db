-- Insert Follower
CREATE OR REPLACE PROCEDURE Insert_user_report(
IN report_id uuid,
IN reporter_id uuid,
IN reported_id uuid
)
LANGUAGE SQL
AS $$
  INSERT INTO user_reports (id, reporter_user_id, reported_user_id)
  VALUES (report_id, reporter_id, reported_id);
$$;
