-- Insert block
CREATE OR REPLACE PROCEDURE Insert_user_block(
    IN blocker_id uuid,
    IN blocked_id uuid
)
    LANGUAGE SQL
AS $$
INSERT INTO user_block (blocker_user_id, blocked_user_id)
VALUES (blocker_id, blocked_id)
ON CONFLICT (blocker_user_id, blocked_user_id)
    DO UPDATE SET deleted_at = null ;
$$;


-- Delete block
CREATE OR REPLACE PROCEDURE Delete_user_block(
IN blocker_id uuid,
IN blocked_id uuid
)
LANGUAGE SQL
AS $$
  UPDATE user_block SET deleted_at = Now() WHERE blocker_user_id = blocker_id AND  blocked_user_id = blocked_id;
$$;

-- Get User Block State
CREATE OR REPLACE FUNCTION Get_Blocked_State(
    IN blocker_user_id_val uuid,
    IN blocked_user_id_val uuid)
    RETURNS TABLE(deleted_at timestamp)
AS $$
SELECT deleted_at FROM user_block WHERE blocker_user_id = blocker_user_id_val AND blocked_user_id = blocked_user_id_val;
$$ LANGUAGE SQL;