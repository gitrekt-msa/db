CREATE TABLE IF NOT EXISTS user_block
(
  blocker_user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  blocked_user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  created_at TIMESTAMP DEFAULT Now(),
  updated_at TIMESTAMP DEFAULT Now(),
  deleted_at TIMESTAMP,
  PRIMARY KEY (blocker_user_id, blocked_user_id)
);
