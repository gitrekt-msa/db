CREATE TABLE IF NOT EXISTS user_reports
(
  id uuid PRIMARY KEY,
  reporter_user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  reported_user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  created_at TIMESTAMP DEFAULT Now(),
  updated_at TIMESTAMP DEFAULT Now(),
  deleted_at TIMESTAMP
);
