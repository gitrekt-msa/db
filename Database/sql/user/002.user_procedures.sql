
-- Insert USER
CREATE OR REPLACE PROCEDURE Insert_User(
  IN id_val uuid,
 	IN email_val varchar(50),
  IN username_val varchar(50),
 	IN password_val varchar(512),
  IN first_name_val varchar(30),
  IN last_name_val varchar(30)
)
LANGUAGE SQL
AS $$
  INSERT INTO users (id, email, username, password, first_name, last_name) 
    VALUES (id_val, email_val, username_val, password_val, first_name_val, last_name_val);
$$;

-- Get User Password
CREATE OR REPLACE FUNCTION Get_User_ID_And_Password(
  IN email_val VARCHAR(50)
) RETURNS TABLE(id UUID, password VARCHAR(512))
  AS $$ SELECT id, password FROM users WHERE email = email_val $$ LANGUAGE SQL;

-- Get User Profile Info
CREATE OR REPLACE FUNCTION Get_User_Profile_Info(
  IN user_id uuid
) RETURNS TABLE(id UUID, username varchar(50), first_name varchar(50), last_name varchar(50))
AS $$
SELECT id, username, first_name, last_name FROM users WHERE id = user_id;
$$ LANGUAGE SQL;


-- Edit USER
CREATE OR REPLACE PROCEDURE Edit_User(
IN id_val uuid,
IN email_val varchar(50),
IN username_val varchar(50),
IN password_val varchar(512),
IN first_name_val varchar(30),
IN last_name_val varchar(30)
)
LANGUAGE SQL
AS $$
  UPDATE users SET username = username_val, first_name = first_name_val, last_name = last_name_val, password = password_val, email = email_val WHERE id = id_val;
$$;
