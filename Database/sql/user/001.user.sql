-- CREATE USER TABLE(s)
CREATE TABLE IF NOT EXISTS users(
  id uuid PRIMARY KEY,
  email varchar(50) UNIQUE,
  username varchar(50) UNIQUE,
  password varchar(512),
  first_name varchar(30),
  last_name varchar(30),
  certified boolean default false
);

