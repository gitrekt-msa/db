FROM postgres

ENV POSTGRES_DB=gitrekt_quora
ENV POSTGRES_USER=root
ENV POSTGRES_PASSWORD=root

COPY . /db
WORKDIR /db

RUN ["sh", "-c", "./db.sh"]
EXPOSE 5432
